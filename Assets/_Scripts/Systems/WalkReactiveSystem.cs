﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace NetDragon.UnityECSAndJobSystemTutorial
{
    [UpdateInGroup(typeof(MoveGroup))]
    public class WalkReactiveSystem : ComponentSystem
    {
        public struct InternalWalk : IComponentData
        {
            public quaternion forward;
        }

        struct AddedWalkData
        {
            public readonly int Length;
            public EntityArray entities;
            public ComponentDataArray<Walk> Walks;
            [ReadOnly] public ComponentDataArray<Rotation> Rotations;
            public SubtractiveComponent<InternalWalk> InternalWalks;
        }
        [Inject] AddedWalkData addedWalkData;

        struct RemovedWalkData
        {
            public readonly int Length;
            public EntityArray entities;
            public SubtractiveComponent<Walk> Walks;
            public ComponentDataArray<InternalWalk> InternalWalks;
        }
        [Inject] RemovedWalkData removedWalkData;

        protected override void OnUpdate()
        {
            for (int i = 0; i < addedWalkData.Length; i++)
            {
                PostUpdateCommands.AddComponent(addedWalkData.entities[i], new InternalWalk()
                {
                    forward = addedWalkData.Rotations[i].Value,
                });
            }

            for (int i = 0; i < removedWalkData.Length; i++)
            {
                PostUpdateCommands.RemoveComponent<InternalWalk>(removedWalkData.entities[i]);
            }

        }
    }
}
