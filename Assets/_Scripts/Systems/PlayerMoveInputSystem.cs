﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace NetDragon.UnityECSAndJobSystemTutorial
{
    [UpdateInGroup(typeof(InputGroup))]
    public class PlayerMoveInputSystem : ComponentSystem
    {
        struct PlayerMoveData
        {
            public readonly int Length;
            [ReadOnly] public ComponentDataArray<Player> Players;
            public ComponentDataArray<Move> Moves;
        }

        [Inject]
        PlayerMoveData playerMoveData;

        protected override void OnUpdate()
        {
            if (playerMoveData.Length == 0)
            {
                return;
            }

            var playerMove = playerMoveData.Moves[0];
            playerMove.direction = new float3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
            playerMoveData.Moves[0] = playerMove;

        }
    }
}
