﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Collections;
using UnityEngine.Jobs;

namespace NetDragon.UnityECSAndJobSystemTutorial
{
    [UpdateInGroup(typeof(InitGroup))]
    public class CopyTransformFromGameObjectSystem : JobComponentSystem
    {
        struct CopyTransformFromGameobjectData
        {
            [WriteOnly] public ComponentDataArray<Position> Positions;
            [WriteOnly] public ComponentDataArray<Rotation> Rotations;
            public TransformAccessArray transforms;
        }

        [Inject]
        CopyTransformFromGameobjectData copyTransformFromGameobjectData;

        struct CopyTransformFromGameobjectJob : IJobParallelForTransform
        {
            [WriteOnly] public ComponentDataArray<Position> Positions;
            [WriteOnly] public ComponentDataArray<Rotation> Rotations;

            public void Execute(int index, TransformAccess transform)
            {
                Positions[index] = new Position()
                {
                    Value = transform.position,
                };
                Rotations[index] = new Rotation()
                {
                    Value = transform.rotation,
                };
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var copyTransformFromGameobjectJob = new CopyTransformFromGameobjectJob()
            {
                Positions = copyTransformFromGameobjectData.Positions,
                Rotations = copyTransformFromGameobjectData.Rotations,
            };
            return copyTransformFromGameobjectJob.Schedule(copyTransformFromGameobjectData.transforms, inputDeps);
        }
    }
}
