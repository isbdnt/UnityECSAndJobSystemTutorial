﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;
using Unity.Burst;

namespace NetDragon.UnityECSAndJobSystemTutorial
{
    [UpdateAfter(typeof(WalkReactiveSystem))]
    [UpdateInGroup(typeof(MoveGroup))]
    public class WalkSystem : JobComponentSystem
    {
        struct WalkData
        {
            public readonly int Length;
            [ReadOnly] public ComponentDataArray<Walk> Walks;
            public ComponentDataArray<WalkReactiveSystem.InternalWalk> InternalWalks;
            [ReadOnly] public ComponentDataArray<Move> Moves;
            public ComponentDataArray<Position> Positions;
            public ComponentDataArray<Rotation> Rotations;
        }

        [Inject]
        WalkData walkData;

        [BurstCompile]
        struct WalkJob : IJobParallelFor
        {
            public float DeltaTime;
            [ReadOnly] public ComponentDataArray<Walk> Walks;
            public ComponentDataArray<WalkReactiveSystem.InternalWalk> InternalWalks;
            [ReadOnly] public ComponentDataArray<Move> Moves;
            public ComponentDataArray<Position> Positions;
            public ComponentDataArray<Rotation> Rotations;

            public void Execute(int index)
            {
                Positions[index] = new Position()
                {
                    Value = Positions[index].Value + Moves[index].direction * Moves[index].speed,
                };
                var internalWalk = InternalWalks[index];
                if (Moves[index].direction.Equals(new float3(0f, 0f, 0f)) == false)
                {
                    internalWalk.forward = quaternion.lookRotation(Moves[index].direction, new float3(0f, 1f, 0f));
                    InternalWalks[index] = internalWalk;
                }
                Rotations[index] = new Rotation()
                {
                    Value = Quaternion.Slerp(Rotations[index].Value, internalWalk.forward, DeltaTime * Walks[index].rotateSpeed),
                };
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var walkJob = new WalkJob()
            {
                DeltaTime = Time.fixedDeltaTime,
                Walks = walkData.Walks,
                InternalWalks = walkData.InternalWalks,
                Moves = walkData.Moves,
                Positions = walkData.Positions,
                Rotations = walkData.Rotations,
            };
            return walkJob.Schedule(walkData.Length, 64, inputDeps);
        }
    }
}
