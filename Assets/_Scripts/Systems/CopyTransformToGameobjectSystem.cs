﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Collections;
using UnityEngine.Jobs;

namespace NetDragon.UnityECSAndJobSystemTutorial
{
    [UpdateInGroup(typeof(ViewGroup))]
    public class CopyTransformToGameobjectSystem : JobComponentSystem
    {
        struct CopyTransformToGameobjectData
        {
            [ReadOnly] public ComponentDataArray<Position> Positions;
            [ReadOnly] public ComponentDataArray<Rotation> Rotations;
            [WriteOnly] public TransformAccessArray transforms;
        }

        [Inject]
        CopyTransformToGameobjectData copyTransformToGameobjectData;

        struct CopyTransformToGameobjectJob : IJobParallelForTransform
        {
            [ReadOnly] public ComponentDataArray<Position> Positions;
            [ReadOnly] public ComponentDataArray<Rotation> Rotations;

            public void Execute(int index, TransformAccess transform)
            {
                transform.position = Positions[index].Value;
                transform.rotation = Rotations[index].Value;
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var copyTransformToGameobjectJob = new CopyTransformToGameobjectJob()
            {
                Positions = copyTransformToGameobjectData.Positions,
                Rotations = copyTransformToGameobjectData.Rotations,
            };
            return copyTransformToGameobjectJob.Schedule(copyTransformToGameobjectData.transforms, inputDeps);
        }
    }
}
