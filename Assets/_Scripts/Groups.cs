﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

namespace NetDragon.UnityECSAndJobSystemTutorial
{
    [UpdateBefore(typeof(FixedUpdate.PhysicsFixedUpdate))]
    [UpdateInGroup(typeof(FixedUpdate))]
    public class InitGroup { }

    [UpdateAfter(typeof(InitGroup))]
    [UpdateBefore(typeof(FixedUpdate.PhysicsFixedUpdate))]
    [UpdateInGroup(typeof(FixedUpdate))]
    public class InputGroup { }

    [UpdateAfter(typeof(InputGroup))]
    [UpdateBefore(typeof(FixedUpdate.PhysicsFixedUpdate))]
    [UpdateInGroup(typeof(FixedUpdate))]
    public class MoveGroup { }

    [UpdateAfter(typeof(MoveGroup))]
    [UpdateBefore(typeof(FixedUpdate.PhysicsFixedUpdate))]
    [UpdateInGroup(typeof(FixedUpdate))]
    public class ViewGroup { }
}
