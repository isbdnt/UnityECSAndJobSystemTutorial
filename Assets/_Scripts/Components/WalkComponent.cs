﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using System;
namespace NetDragon.UnityECSAndJobSystemTutorial
{
    [Serializable]
    public struct Walk : IComponentData
    {
        public float rotateSpeed;
    }
    public class WalkComponent : ComponentDataWrapper<Walk> { }
}
