﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

namespace NetDragon.UnityECSAndJobSystemTutorial
{
    public struct Player : IComponentData
    {
    }

    public class PlayerComponent : ComponentDataWrapper<Player> { }
}
