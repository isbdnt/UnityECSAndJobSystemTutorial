﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using System;

namespace NetDragon.UnityECSAndJobSystemTutorial
{
    [Serializable]
    public struct Move : IComponentData
    {
        public float3 direction;
        public float speed;
    }

    public class MoveComponent : ComponentDataWrapper<Move> { }
}
